package com.fc2.blog.symfoware.jjserver;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import jdk.nashorn.api.scripting.ScriptObjectMirror;
import net.freeutils.httpserver.HTTPServer;
import net.freeutils.httpserver.HTTPServer.VirtualHost;
import net.freeutils.httpserver.HTTPServer.ContextHandler;
import net.freeutils.httpserver.HTTPServer.FileContextHandler;
import net.freeutils.httpserver.HTTPServer.Request;
import net.freeutils.httpserver.HTTPServer.Response;

public class MainProcess implements ContextHandler {

	protected final File apiBase;
	
	public MainProcess(File base) throws IOException {
		this.apiBase = new File(base.getCanonicalFile(), "api");
	}
	
	@Override
	public int serve(Request req, Response resp) throws IOException {
		
		resp.getHeaders().add("Content-Type", "application/json");
		
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("javascript");
		
		try {
			List<String> paths = parserPath(req.getPath());
			
			
			// bodyはjsonであることを期待
			ScriptObjectMirror json = (ScriptObjectMirror) engine.eval("JSON");
			Object result = json.callMember("parse", streamToString(req.getBody()));
			
			engine.eval(new FileReader(new File(this.apiBase, paths.get(1) + ".js")));
			
			String method = paths.size() < 3 ? "index" : paths.get(2);
			Invocable inv = (Invocable) engine;
			Object res = inv.invokeFunction(method, result);
			
			resp.send(200, res.toString());
			
		} catch(Exception e) {
			e.printStackTrace();
			throw new IOException(e);
		}
		
		
		return 0;
	}
	
	private String streamToString(InputStream is) throws IOException {
		@SuppressWarnings("resource")
		Scanner s = new Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "[]";
	}
	
	private List<String> parserPath(String fullpath) {
		
		List<String> result = new ArrayList<>();
		String[] paths = fullpath.split("/");
		for(String path : paths) {
			if ("".equals(path)) {
				continue;
			}
			if ("index.html".equals(path)) {
				continue;
			}
			result.add(path);
		}
		
		return result;
		
	}
	
	
	public static void main(String... args) throws Exception {
		
		String baseDir = args.length < 1 ? "": args[0];
		int port = args.length < 2 ? 8080 : Integer.parseInt(args[1]);
		
		HTTPServer server = new HTTPServer(port);
		
		// デフォルトホスト
		VirtualHost host = server.getVirtualHost(null);
		
		// ディレクトリの表示を許可
		host.setAllowGeneratedIndex(true);
		
		// 「/」アクセスで、カレントディレクトリ表示
		File dir = new File(baseDir);
		host.addContext("/", new FileContextHandler(dir));
		
		// 「/api/」アクセスで、プログラム実行
		host.addContext("/api/", new MainProcess(dir), new String[]{"GET", "POST"});
		
		// サーバー起動
		server.start();
		System.out.println("jjserver is listening on port " + port);
		
	}

}
